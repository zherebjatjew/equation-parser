package org.equation.paser.impl;

import org.equation.paser.api.EquationNode;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

class EquationParserImplTest {
    private final EquationParserImpl parser = new EquationParserImpl(new OperationConstants());

    @Test
    public void doTest() throws URISyntaxException, IOException {
        URL resource = getClass().getClassLoader().getResource("equation-parser-test.csv");
        assertNotNull(resource, "File not found");
        File file = new File(resource.toURI());
        int position = 1;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; position++) {
                if (line.isEmpty() || line.startsWith("#")) {
                    continue;
                }
                String[] parameters = line.split("\\s*;\\s*");
                assertEquals(3, parameters.length, "Error in line " + position + ": 3 parameters expected");
                EquationNode eq = parser.parse(parameters[0]);
                assertEquals(parameters[1], eq.toString(), parameters[2] + " (line " + position + ')');
            }
        }
    }
}