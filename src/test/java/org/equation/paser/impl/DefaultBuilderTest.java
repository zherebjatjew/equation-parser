package org.equation.paser.impl;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DefaultBuilderTest {

    @Test
    public void toStringOfLeafShouldReturnContent() {
        String value = new DefaultBuilder("content").toString();
        assertEquals("content", value);
    }

    @Test
    public void toStringOgNodeShouldReturnTree() {
        String value = new DefaultBuilder(null)
                .add(new DefaultBuilder("node1"))
                .add(new DefaultBuilder("X")
                        .add(new DefaultBuilder("Y"))).toString();
        assertEquals("->\n  node1\n  ->\n    X\n    Y", value);
    }

}