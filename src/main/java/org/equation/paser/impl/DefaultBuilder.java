package org.equation.paser.impl;

import org.equation.paser.api.custom.EquationTreeBuilder;

import java.util.ArrayList;
import java.util.List;

public class DefaultBuilder implements EquationTreeBuilder {
    private String content;

    private final List<EquationTreeBuilder> children = new ArrayList<>();

    public DefaultBuilder(String content) {
        this.content = content;
    }

    public EquationTreeBuilder of(String content) {
        return new DefaultBuilder(content);
    }

    public EquationTreeBuilder add(EquationTreeBuilder right) {
        if (isLeaf()) {
            EquationTreeBuilder x = new DefaultBuilder(content);
            children.add(x);
            content = null;
        }
        children.add(right);
        return this;
    }

    @Override
    public EquationTreeBuilder placeholder() {
        return new DefaultBuilder("□");
    }

    @Override
    public EquationTreeBuilder toFunction(EquationTreeBuilder arguments) {
        return add(arguments);
    }

    private boolean isLeaf() {
        return content != null;
    }

    @Override
    public String toString() {
        if (isLeaf()) {
            return content;
        } else {
            StringBuilder result = new StringBuilder("->");
            for (EquationTreeBuilder child : children) {
                String item = child.toString();
                String[] lines = item.split("\n");
                for (String ln : lines) {
                    result.append("\n  ");
                    result.append(ln);
                }
            }
            return result.toString();
/*
            return "+\n" + children.stream()
                    .map(node -> node.toString())  // somehow here Stream.toString is called instead of EquationTreeBuilder.toString
                    .map(item -> item.split("\n"))
                    .map(Arrays::stream)
                    .map(line -> "  " + line)
                    .collect(Collectors.joining("\n"));
*/
        }
    }
}
