package org.equation.paser.impl;

public enum EquationOperatorType {
    PLAIN,
    STACK_CENTERED,
    STACK_LEFT,
    STACK_RIGHT,
    SUBSCRIPT,
    SUPERSCRIPT,
    FUNCTION,
    BRACKETS,
    SUB_AND_SUPERSCRIPT
}
