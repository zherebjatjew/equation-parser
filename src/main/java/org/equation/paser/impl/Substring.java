package org.equation.paser.impl;

public class Substring {
    private int position;
    private final String string;

    public Substring(String content) {
        string = content;
        position = 0;
    }

    public int getPos() {
        return position;
    }

    public void advisePos(int offset) {
        position += offset;
        if (position > string.length()) {
            throw new IndexOutOfBoundsException("Position must be within string");
        }
    }

    public boolean hasNext() {
        return position < string.length();
    }

    public char charAt(int index) {
        return string.charAt(index + position);
    }

    public int getOffset(int from) {
        return position - from;
    }

    @Override
    public String toString() {
        return string.substring(position);
    }

    public void increment() {
        position++;
    }
}
