package org.equation.paser.impl;

public class EquationOperator {
    private final String text;
    private final String alias;
    private final int priority;
    private final String closingPair;
    private final boolean parsable;
    private final EquationOperatorType type;

    public EquationOperator(
            String text, String alias, int priority, String closingPair, boolean parsable, EquationOperatorType type)
    {
        this.text = text;
        this.alias = alias;
        this.priority = priority;
        this.closingPair = closingPair;
        this.parsable = parsable;
        this.type = type;
    }

    public String getClosing() {
        return closingPair;
    }

    public int getPriority() {
        return priority;
    }

    public String getText() {
        return text;
    }

    public boolean isUnary() {
        return priority == 0;
    }

    public boolean isFunction() {
        return priority == 0 && closingPair != null;
    }

    public EquationOperatorType getType() {
        return type;
    }

    public boolean isParsable() {
        return parsable;
    }
}
