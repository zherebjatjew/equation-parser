package org.equation.paser.impl;

import org.equation.paser.api.EquationErrorType;
import org.equation.paser.api.EquationParser;
import org.equation.paser.api.EquationNode;
import org.equation.paser.api.custom.FunctionProcessor;

import java.util.Objects;


public class EquationParserImpl implements EquationParser {
    private final OperationConstants constants;
    private double fontSize;

    public EquationParserImpl(OperationConstants constants) {
        this.constants = constants;
    }

    public EquationNode parse(String formula) {
        if (formula == null) {
            throw new IllegalArgumentException("Formula can not be null");
        }
        Substring text = new Substring(formula);
        EquationNode root = readOperand(text, null, null);
        return root;
    }

    @Override
    public void registerFunction(FunctionProcessor processor) {
    }

    private EquationNode readOperand(Substring text, EquationOperator previousOperator, EquationOperator groupOperator) {
        EquationOperator operator = readUnaryOperator(text);
        if (operator != null) {
            text.advisePos(operator.getText().length());
            return wrap(null, operator, readOperand(text, previousOperator, groupOperator));
        }
        EquationNode left = createNode();

        while (text.hasNext()) {
            if (groupOperator != null) {
                String closingBracket = atClosingBracket(text);
                if (closingBracket != null) {
                    // We are in parsing a subsequence (e.g., function arguments), and it is completed.
                    break;
                }
            }
            EquationOperator nextOperator = readBinaryOperator(text);
            if (nextOperator == null) {
                if (!Character.isWhitespace(text.charAt(0))) {
                    left.setContent(left.getContent() + text.charAt(0));
                }
                text.increment();
            } else {
                if (nextOperator.getClosing() != null) {
                    text.advisePos(nextOperator.getText().length());
                    if (nextOperator.isParsable()) {
                        EquationNode right = readOperand(text, null, nextOperator);
                        if (!text.toString().startsWith(nextOperator.getClosing())) {
                            return error(left, EquationErrorType.BRACKET_NOT_CLOSED, nextOperator.getClosing());
                        }
                        text.advisePos(nextOperator.getClosing().length());
                        if (left == null || left.getContent().isEmpty()) {
                            left = right;
                        } else {
                            left = wrap(left, nextOperator, right);
                        }
                    } else {
                        left = readString(text, left, nextOperator.getClosing());
                    }
                } else {
                    if (reverseOrderedOperators(previousOperator, nextOperator)) {
                        text.advisePos(nextOperator.getText().length());
                        left = wrap(left, nextOperator, readOperand(text, nextOperator, groupOperator));
                    } else {
                        if (previousOperator != null) {
                            // continue a chain of peer operators in the upper call
                            break;
                        }
                        text.advisePos(nextOperator.getText().length());
                        left = wrap(left, nextOperator, readOperand(text, nextOperator, groupOperator));
                    }
                }
            }
        }
        if (operator != null) {
            left = wrap(null, operator, left);
        }
        return left;
    }

    private boolean reverseOrderedOperators(EquationOperator previousOperator, EquationOperator nextOperator) {
        return previousOperator != null
                && !previousOperator.isUnary()
                && nextOperator.getPriority() > previousOperator.getPriority();
    }

    private String atClosingBracket(Substring text) {
        return constants.stream()
                .map(EquationOperator::getClosing)
                .filter(Objects::nonNull)
                .filter(s -> text.toString().startsWith(s))
                .findAny().orElse(null);
    }

    private EquationNode readString(Substring text, EquationNode left, String closing) {
        String name = "";
        while (!text.toString().startsWith(closing)) {
            if (!text.hasNext()) {
                left = error(left, EquationErrorType.BRACKET_NOT_CLOSED, "\"");
                return left;
            }
            name += text.charAt(0);
            text.increment();
        }
        text.advisePos(closing.length());
        EquationNode item = explodeString(name);
        if (item == null) {
            left.setContent(OperationConstants.EXP_SYMBOL_QTO + name + OperationConstants.EXP_SYMBOL_QTC);
        } else {
            item = wrap(OperationConstants.EXP_SYMBOL_QTO, item, OperationConstants.EXP_SYMBOL_QTC);
        }
        return wrap(left, null, item);
    }

    private EquationNode wrap(EquationNode left, EquationOperator operator, EquationNode right) {
        if (operator == null && right == null) {
            return left;
        }
        if (operator == null && left == null) {
            return right;
        }
        EquationNode opNode = null;
        if (operator != null) {
            opNode = createNode(operator.getText());
            opNode.setPriority(operator.getPriority());
        }
        if (left == null && right == null) {
            return opNode;
        }
        EquationNode wrapper = createNode();
        wrapper.setLeft(left);
        wrapper.setRight(right);
        wrapper.setOperator(opNode);
        wrapper.setOperatorType(operator == null ? EquationOperatorType.PLAIN : operator.getType());
        return wrapper;
    }

    private EquationNode dispatchItem(EquationNode node) {
        switch (node.getOperatorType()) {
            case PLAIN: {
                break;
            }
            case SUPERSCRIPT: {
                break;
            }
            case STACK_CENTERED: {
                break;
            }
            case BRACKETS: {
                break;
            }
            case SUBSCRIPT: {
                break;
            }
        }
        return node;
    }

    private EquationNode dispatchFunction(EquationNode node) {
        // TODO add custom function processing
        return node;
    }

    private EquationNode error(EquationNode currentNode, EquationErrorType errorType, String description) {
        if (currentNode == null) {
            throw new IllegalArgumentException("Node cannot be null");
        }
        String message = String.format(errorType.getMessage(), description);
        EquationNode result = createNode(message);
        result.setFontSize(getFontSize());
        result.setLeft(currentNode);
        result.setErroneous(true);
        return result;
    }

    private EquationOperator readOperator(Substring text, boolean unary) {
        while (Character.isWhitespace(text.charAt(0))) {
            if (!text.hasNext()) {
                // TODO throw exception
                return null;
            }
            text.increment();
        }
        EquationOperator result = constants.stream()
                .filter(op -> (unary && op.isUnary() && op.getClosing() == null) || (!unary && !op.isUnary()))
                .filter(op -> text.toString().startsWith(op.getText()))
                .findAny()
                .orElse(null);
        return result;
    }

    private EquationOperator readBinaryOperator(Substring text) {
        return readOperator(text, false);
    }

    private EquationOperator readUnaryOperator(Substring text) {
        return readOperator(text, true);
    }

    private EquationNode wrap(String prefix, EquationNode item, String suffix) {
        if (item == null || item.isSimple()) {
            return item;
        }
        EquationNode wrapper = createNode();
        if (prefix != null) {
            wrapper.setLeft(createNode(prefix));
        }
        wrapper.setOperator(item);
        if (suffix != null) {
            wrapper.setRight(createNode(suffix));
        }
        return wrapper;
    }

    public EquationNode explodeString(String name) {
        return null;
    }

    private EquationNode createNode() {
        return createNode("");
    }

    private EquationNode createNode(String content) {
        EquationNode result = new EquationNode();
        result.setContent(content);
        return result;
    }

    public double getFontSize() {
        return fontSize;
    }

    public void setFontSize(double fontSize) {
        this.fontSize = fontSize;
    }
}
