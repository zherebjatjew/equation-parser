package org.equation.paser.impl;

import java.util.ArrayList;

public class OperationConstants extends ArrayList<EquationOperator> {
    public static final String EXP_SYMBOL_MINUS   = "−";
    public static final String EXP_SYMBOL_MULT    = "×";
    public static final String EXP_SYMBOL_EQ      = "≡";
    public static final String EXP_SYMBOL_GE      = "≥";
    public static final String EXP_SYMBOL_LE      = "≤";
    public static final String EXP_SYMBOL_NE      = "≠";
    public static final String EXP_SYMBOL_NOT     = "¬";
    public static final String EXP_SYMBOL_QTO     = "«";
    public static final String EXP_SYMBOL_QTC     = "»";
    public static final String EXP_SYMBOL_ROOT    = "√";
    public static final String EXP_SYMBOL_SUMM    = "Σ";
    public static final String EXP_SYMBOL_PI      = "π";
    public static final String EXP_SYMBOL_EMPTY   = "□";
    public static final String EXP_SYMBOL_AND     = "∧";
    public static final String EXP_SYMBOL_OR      = "∨";

    public OperationConstants() {
        add(new EquationOperator("!=", EXP_SYMBOL_NE, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("+", null, 0, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("-", EXP_SYMBOL_MINUS, 0, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("!", EXP_SYMBOL_NOT, 0, null, true, EquationOperatorType.PLAIN));

        add(new EquationOperator(";", null, 5, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator(",", null, 5, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("&", EXP_SYMBOL_AND, 8, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("|", EXP_SYMBOL_OR, 8, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("<=", EXP_SYMBOL_LE, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator(">=", EXP_SYMBOL_GE, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("==", EXP_SYMBOL_EQ, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("=", null, 8, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("<", null, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator(">", null, 9, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("+", null, 10, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("-", EXP_SYMBOL_MINUS, 10, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("*", EXP_SYMBOL_MULT, 11, null, true, EquationOperatorType.PLAIN));
        add(new EquationOperator("/", null, 11, null, true, EquationOperatorType.STACK_CENTERED));
        add(new EquationOperator("^", null, 12, null, true, EquationOperatorType.SUPERSCRIPT));

        add(new EquationOperator("(", null, 100, ")", true, EquationOperatorType.BRACKETS));
        add(new EquationOperator("[", null, 100, "]", true, EquationOperatorType.BRACKETS));
        add(new EquationOperator("\"", null, 100, "\"", false, EquationOperatorType.BRACKETS));
    }
}
