package org.equation.paser.api.custom;


public interface FunctionProcessor {
    EquationTreeBuilder process(String name, EquationTreeBuilder ... arguments);
}
