package org.equation.paser.api.custom;


public interface EquationTreeBuilder {
    EquationTreeBuilder of(String content);
    EquationTreeBuilder add(EquationTreeBuilder right);
    EquationTreeBuilder placeholder();

    /**
     * Re-format the builder to a function. The builder should be a leaf (or should be convertible to leaf that means
     * it has only one non-empty leaf of convertible to leaf child. This final leaf represents the name of the function.
     *
     * @param arguments list of nodes. each of them is a separate argument of the function. argument can be either
     *                  a leaf or a compound node
     * @return a builder with a new structure:
     * <code>
     *   {
     *       function name,
     *       {
     *           argument1,
     *           argument2,
     *           ...
     *       }
     *   }
     * </code>
     */
    EquationTreeBuilder toFunction(EquationTreeBuilder arguments);
}
