package org.equation.paser.api;

public interface ExpressionTransformingStrategy {
    EquationNode transform(EquationNode expression);
}
