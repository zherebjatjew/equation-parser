package org.equation.paser.api;


import org.equation.paser.impl.EquationOperatorType;

import java.util.EnumSet;

public class EquationNode {
    private String content;
    private double fontSize;
    private EquationNode left;
    private EquationNode operator;
    private EquationNode right;
    private double verticalOffset;
    private final EnumSet<EquationNodeOptions> options = EnumSet.noneOf(EquationNodeOptions.class);
    private EquationOperatorType operatorType;
    private int priority;

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    private enum EquationNodeOptions {
        UNDERLINED, OVERLINED, ERRONEOUS, CORRECT
    }

    public EquationNode() {}

    public EquationNode(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getFontSize() {
        return fontSize;
    }

    public void setFontSize(double fontSize) {
        this.fontSize = fontSize;
    }

    public EquationNode getLeft() {
        return left;
    }

    public void setLeft(EquationNode left) {
        this.left = left;
    }

    public EquationNode getOperator() {
        return operator;
    }

    public void setOperator(EquationNode operator) {
        this.operator = operator;
    }

    public EquationNode getRight() {
        return right;
    }

    public void setRight(EquationNode right) {
        this.right = right;
    }

    public double getVerticalOffset() {
        return verticalOffset;
    }

    public void setVerticalOffset(double verticalOffset) {
        this.verticalOffset = verticalOffset;
    }

    public boolean isUnderlined() {
        return options.contains(EquationNodeOptions.UNDERLINED);
    }

    public void setUnderlined(boolean value) {
        if (value) {
            options.add(EquationNodeOptions.UNDERLINED);
        } else {
            options.remove(EquationNodeOptions.UNDERLINED);
        }
    }

    public boolean isOverlined() {
        return options.contains(EquationNodeOptions.UNDERLINED);
    }

    public void setOverlined(boolean value) {
        if (value) {
            options.add(EquationNodeOptions.OVERLINED);
        } else {
            options.remove(EquationNodeOptions.OVERLINED);
        }
    }

    public boolean getErroneous() {
        return options.contains(EquationNodeOptions.ERRONEOUS);
    }

    public void setErroneous(boolean erroneous) {
        if (erroneous) {
            options.add(EquationNodeOptions.ERRONEOUS);
        } else {
            options.remove(EquationNodeOptions.ERRONEOUS);
        }
    }

    public EquationOperatorType getOperatorType() {
        return operatorType;
    }

    public void setOperatorType(EquationOperatorType operatorType) {
        this.operatorType = operatorType;
    }

    public boolean isSimple() {
        return left == null && right == null && operator == null;
    }

    @Override
    public String toString() {
        String result = content;
        if (left != null || operator != null || right != null) {
            result += "{" + (left == null ? "" : left.toString()) +
                    ", " + (operator == null ? "" : operator.toString()) +
                    ", " + (right == null ? "" : right.toString()) + "}";
        }
        return result;
    }
}
