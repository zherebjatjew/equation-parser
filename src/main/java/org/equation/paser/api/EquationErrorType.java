package org.equation.paser.api;

public enum EquationErrorType {
    BRACKET_NOT_CLOSED("'%s' expected "),
    EXTRA_TEXT("Unexpected text '%s' "),
    ARGUMENT_EXPECTED("Argument expected: '%s' ");

    private final String message;

    private EquationErrorType(String message) {
        this.message = message;
    }


    public String getMessage() {
        return message;
    }
}
