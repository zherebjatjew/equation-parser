package org.equation.paser.api;

import org.equation.paser.api.custom.FunctionProcessor;

public interface EquationParser {
    EquationNode parse(String formula);

    void registerFunction(FunctionProcessor processor);
}
