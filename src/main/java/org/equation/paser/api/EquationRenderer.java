package org.equation.paser.api;

public interface EquationRenderer {
    void render(EquationNode root);
}
